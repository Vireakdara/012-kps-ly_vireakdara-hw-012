import React from 'react';
import './Component/Style/App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './Component/Navbar/Menu';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'


import notFound from './Component/Navbar/notFound';

import {AppLayout} from './Component/Navbar/Auth/welcome'
import {ProtectedRoute} from './Component/Navbar/Auth/Protect.route'

import reactRouter from './Component/Navbar/React-Router/reactRouter';
import Home from './Component/Navbar/Home/Home';
import View from './Component/Navbar/Home/View';
import View1 from './Component/Navbar/Home/View1';
import View2 from './Component/Navbar/Home/View2';
import Video from './Component/Navbar/Video/Video';
import Account from './Component/Navbar/Account/Account';
import Auth from './Component/Navbar/Auth/Auth';



function App() {
  return (
    <div className="App">
      <Router>
        <Menu />
          <h1></h1>
          <Switch>


            <Route path='/' exact component={reactRouter} />
            <Route path='/Home' component={Home} />
            <Route path='/Video' component={Video} />
            <Route path='/Account' component={Account} />
            <Route path='/Auth' component={Auth} />
            <ProtectedRoute  path="/Welcome" component={AppLayout} />

            <Route path='/posts/1' component={View} />
            <Route path='/posts/2' component={View1} />
            <Route path='/posts/3' component={View2} />
            <Route path='*' component={notFound} />



          </Switch>
      </Router>  
    </div>
  );
}

export default App;
