import React from "react";
import { Button} from 'react-bootstrap'
import auth from "./logIn";

export const AppLayout = props => {
  return (
    <div className='container'>
      <h1>Welcome</h1>
      <Button variant="primary"
        onClick={() => {
          auth.logout(() => {
            props.history.push("/Auth");
          });
        }}
      >
        Log out
      </Button>
    </div>
  );
};
