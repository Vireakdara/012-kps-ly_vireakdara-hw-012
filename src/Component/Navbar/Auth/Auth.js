import React from 'react'
import {Form, Button} from 'react-bootstrap'
import logIn from './logIn'

function Auth(props) {
    return (
        <div className='container'>
            <Form>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Name</Form.Label>
                <Form.Control type="Name" placeholder="Enter Name" />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
            </Form.Group>
            <Button variant="primary" onClick={() => {
                        logIn.login(() => {
                            props.history.push("/Welcome");
                        });
                    }}  >
                Submit
            </Button>
            </Form>
        </div>
    )
}

export default Auth
