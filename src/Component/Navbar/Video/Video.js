import React from 'react'
import {BrowserRouter as Router ,Link, useParams, useRouteMatch, Switch, Route} from 'react-router-dom'

function Video() {
    return (
        <div className='container'>
            <Router>
                <div>
                    <ul>
                        <li>
                            <Link to="/Video/animation">Animation</Link>
                        </li>
                        <li>
                            <Link to="/Video/movie">Movie</Link>
                        </li>  
                    </ul>
                    <Switch>
                        <Route exact path="/Video">
                            <Home />
                        </Route>
                        <Route path="/Video/animation">
                            <Animations />
                        </Route>
                        <Route path="/Video/movie">
                            <Movies />
                        </Route>
                    </Switch>
                </div>
            </Router>
        </div>
    )
}

function Home() {
    return (
      <div>
        <h2>Please Select A Topic</h2>
      </div>
    );
  }

function Animations() {
    // The `path` lets us build <Route> paths that are
    // relative to the parent route, while the `url` lets
    // us build relative links.
    let { path, url } = useRouteMatch();
  
    return (
      <div>
        <h2>Animation Category</h2>
        <ul>
          <li>
            <Link to={`${url}/action`}>Action</Link>
          </li>
          <li>
            <Link to={`${url}/romance`}>Romance</Link>
          </li>
          <li>
            <Link to={`${url}/comedy`}>Comedy</Link>
          </li>
        </ul>
  
        <Switch>
          <Route exact path={path}>
            <h4>Please select a topic.</h4>
          </Route>
          <Route path={`${path}/:animationId`}>
            <Animation />
          </Route>
        </Switch>
      </div>
    );
  }
  
  function Animation() {
    // The <Route> that rendered this component has a
    // path of `/topics/:animationId`. The `:animationId` portion
    // of the URL indicates a placeholder that we can
    // get from `useParams()`.
    let { animationId } = useParams();
  
    return (
      <div>
        <h3>{animationId}</h3>
      </div>
    );
  }

  function Movies(){
      let {path, url} = useRouteMatch();

      return(
          <div>
            <h2>Movie Category</h2>
                <ul>
                <li>
                    <Link to={`${url}/adventure`}>Adventure</Link>
                </li>
                <li>
                    <Link to={`${url}/comedy`}>Comedy</Link>
                </li>
                <li>
                    <Link to={`${url}/crime`}>Crime</Link>
                </li>
                <li>
                    <Link to={`${url}/documentary`}>Documentary</Link>
                </li>
                </ul>

                <Switch>
                <Route exact path={path}>
                    <h4>Please select a topic.</h4>
                </Route>
                <Route path={`${path}/:movieId`}>
                    <Movie />
                </Route>
                </Switch>
        </div>
        
      );
  }

  function Movie() {
    let { movieId } = useParams();
    return (
      <div>
        <h3>{movieId}</h3>
      </div>
    );
  }








export default Video    
