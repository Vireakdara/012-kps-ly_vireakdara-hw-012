import React from 'react'

function View1() {
    return (
        <div className='container'>
            <h1>This is content from post 2</h1>
        </div>
    )
}

export default View1
