import React from 'react'

function View() {
    return (
        <div className='container'>
            <h1>This is content from post 1</h1>
        </div>
    )
}

export default View
