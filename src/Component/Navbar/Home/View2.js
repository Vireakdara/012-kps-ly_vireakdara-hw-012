import React from 'react'

function View2() {
    return (
        <div className='container'>
            <h1>This is content from post 3</h1>
        </div>
    )
}

export default View2
