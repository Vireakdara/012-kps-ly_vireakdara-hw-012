import React from 'react'
import {CardDeck, Card, } from 'react-bootstrap'
import {Link} from 'react-router-dom'

function Home() {
    return (
        <div className='container'>
            <div className='row'>
                <div className='col-lg-12'>
                    <CardDeck>
                        <Card>
                            <Card.Img variant="top" src="https://i.pinimg.com/originals/4c/58/fa/4c58fad9d539c8e6a6e70aa1decd8f17.png" />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.
                            </Card.Text>
                            <Card.Link as={Link} to={`/posts/1`} >See more</Card.Link>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src="https://specials-images.forbesimg.com/imageserve/5ea0466e9869020006ff53d7/960x0.jpg?cropX1=0&cropX2=1280&cropY1=0&cropY2=720" />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.
                            </Card.Text>
                            <Card.Link as={Link} to={`/posts/2`}>See more</Card.Link>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src="https://i.ytimg.com/vi/GHNld7mz_EA/maxresdefault.jpg" />
                            <Card.Body>
                            <Card.Title>Card title</Card.Title>
                            <Card.Text>
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.
                            </Card.Text>
                            <Card.Link as={Link} to={`/posts/3`}>See more</Card.Link>
                            </Card.Body>
                            <Card.Footer>
                            <small className="text-muted">Last updated 3 mins ago</small>
                            </Card.Footer>
                        </Card>
                    </CardDeck>
                </div>
            </div>
        </div>
    )
}

export default Home
